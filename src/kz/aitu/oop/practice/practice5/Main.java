package kz.aitu.oop.practice.practice5;

/**
 * main class we will call app.
 */
public class Main {
    public static void main(String[] args) {
        App app = new App();
        app.start();
    }
}

