/**
 * creating a chain with random characteristics.
 */
package kz.aitu.oop.practice.practice5.Creating;

import kz.aitu.oop.practice.practice5.SpecificationsStone.Fineness;
import kz.aitu.oop.practice.practice5.SpecificationsStone.MaterialChain;

import java.util.Random;

public class CreateChain {
public static Chain createChain(){
    Random r = new Random();
    return new Chain(
            MaterialChain.values()[(new Random().nextInt(MaterialChain.values().length))],
            r.nextDouble() * 10 +0.1,
            Fineness.values()[(new Random().nextInt(Fineness.values().length))]);
}
}
