/**
 * this is a class for Necklace;
 * here we add stones and a chainж
 */
package kz.aitu.oop.practice.practice5.Creating;

import kz.aitu.oop.practice.practice5.Stone.Stone;

import java.util.ArrayList;
import java.util.List;

public class Necklace {
    private List<Stone> stonesList = new ArrayList<Stone>();
    private Chain chain;

    public Necklace() {

    }

    public Stone getStone(int position){
        return stonesList.get(position);
    }

    public List<Stone> getStonesList() {
        return stonesList;
    }



    public void setStonesList(List<Stone> stonesList) {
        this.stonesList = stonesList;
    }

    public Chain getChain() {
        return chain;
    }

    public void setChain(Chain chain) {
        this.chain = chain;
    }

    @Override
    public String toString() {
        return "\nCreate Necklace:\n" +
                chain +
                ", stones:  " + stonesList +
                '\n';
    }
}
