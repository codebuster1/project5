/**
 * creating stones for a specific query.
 */
package kz.aitu.oop.practice.practice5.Creating;

import kz.aitu.oop.practice.practice5.SpecificationsStone.FullPreciousStoneType;
import kz.aitu.oop.practice.practice5.SpecificationsStone.HalfPreciousStoneType;
import kz.aitu.oop.practice.practice5.SpecificationsStone.Sparkle;
import kz.aitu.oop.practice.practice5.SpecificationsStone.StoneColor;
import kz.aitu.oop.practice.practice5.Stone.FullPreciousStone;
import kz.aitu.oop.practice.practice5.Stone.HalfPreciousStone;
import kz.aitu.oop.practice.practice5.Stone.Stone;

import java.util.Random;

public class CreateChStone {
    public static Stone stoneCreate(int choice) {
        Random r = new Random();
        switch (choice) {
            case 1:
                return new FullPreciousStone(
                        r.nextDouble() * 10 + 0.1,
                        (r.nextDouble() * 10 + 0.1) * 1000,
                        // we have enums with different colors,StoneName, we randomly choose one of them.
                        StoneColor.values()[(new Random().nextInt(StoneColor.values().length))],
                        FullPreciousStoneType.values()[(new Random().nextInt(FullPreciousStoneType.values().length))]
                );
            case 2:
                return new HalfPreciousStone(
                        r.nextDouble() * 10 + 0.1,
                        (r.nextDouble() * 10 + 0.1) * 1000,
                        // we have enums with different colors,StoneName, we randomly choose one of them.
                        StoneColor.values()[(new Random().nextInt(StoneColor.values().length))],
                        HalfPreciousStoneType.values()[(new Random().nextInt(HalfPreciousStoneType.values().length))],
                        Sparkle.values()[(new Random().nextInt(Sparkle.values().length))]
                );
            default:
                throw new IllegalArgumentException();
        }

    }
}
