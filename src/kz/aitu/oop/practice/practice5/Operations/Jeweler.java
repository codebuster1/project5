/**
 *  this class is responsible for creating the necklace.
 */
package kz.aitu.oop.practice.practice5.Operations;

import kz.aitu.oop.practice.practice5.Creating.CreateChStone;
import kz.aitu.oop.practice.practice5.Creating.CreateChain;
import kz.aitu.oop.practice.practice5.Creating.Necklace;
import kz.aitu.oop.practice.practice5.Creating.СreateRStone;
import kz.aitu.oop.practice.practice5.Stone.Stone;

import java.util.ArrayList;
import java.util.List;


public class Jeweler {

    public Necklace createNecklace(Necklace necklace, int countStone, int choiceStone,int choice1) {
        List<Stone> stones = new ArrayList<Stone>();
        switch (choiceStone) {
            case 1:
                for (int i = 0; i < countStone; i++) {
                    stones.add(СreateRStone.stoneCreate());
                }
                break;

            case 2:
                for (int i = 0; i < countStone; i++) {
                    stones.add(CreateChStone.stoneCreate(choice1));
                }
                break;


        }

        necklace.setStonesList(stones);
        necklace.setChain(CreateChain.createChain());
        return necklace;
    }


}
