/**
 * This class calculates the weight and price of stones.
 */
package kz.aitu.oop.practice.practice5.Operations;

import kz.aitu.oop.practice.practice5.Stone.Stone;

import java.util.List;

public class Calculate {
    public static double calculateTotalCaratWeight(List<Stone> stones) {
        double totalCaratWeight = 0.0;
        for (Stone stone : stones) {
            totalCaratWeight += stone.getWeight();
        }
        return totalCaratWeight;
    }

    public static int calculateTotalStonesCost(List<Stone> stones) {
        int totalCost = 0;
        for (Stone stone : stones) {
            totalCost += stone.getPrice() * stone.getWeight();
        }

        return totalCost;
    }
}
