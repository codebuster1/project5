package kz.aitu.oop.practice.practice5.SpecificationsStone;
public enum HalfPreciousStoneType {
    Alexandrite, Agate, Amethyst, Aquamarine, Garnet, Lapis;
}
