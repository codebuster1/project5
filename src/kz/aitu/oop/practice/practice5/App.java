package kz.aitu.oop.practice.practice5; /**
 * app for the user.
 */

import kz.aitu.oop.practice.practice5.Creating.Necklace;
import kz.aitu.oop.practice.practice5.Operations.Jeweler;
import kz.aitu.oop.practice.practice5.Operations.Calculate;

import java.util.Scanner;

public class App {
    public void start() {
        System.out.println("\nWelcome to RandomJewerlly ");
        Necklace necklace1 = new Necklace();
        Jeweler batya = new Jeweler();
        int choice, quantityStone;

        Scanner in = new Scanner(System.in);

        while (true) {
            System.out.println("Choice one operation:\n");
            System.out.println("1.Create necklace :D");
            System.out.println("2.Calculate total weight O_O");
            System.out.println("3.Calculate total cost +_+");

            choice = in.nextInt();
            switch (choice) {
                case 1: {
                    System.out.println("Choice one operation:\n");
                    System.out.println("1.Create necklace with random stones");
                    System.out.println("2.Create necklace with choice precious stones");
                    int choiceStone = in.nextInt();
                    switch (choiceStone) {

                        case 1:
                            System.out.println("Enter the number of stones on a necklace");
                            quantityStone = in.nextInt();
                            batya.createNecklace(necklace1, quantityStone, choiceStone, 0);
                            System.out.println(necklace1 + "\n");
                            System.out.println("GET DESCRIPTION enter positions: ");
                            necklace1.getStone(in.nextInt() - 1).infoAboutStone();
                            break;

                        case 2:
                            System.out.println("1.Create necklace with full stones");
                            System.out.println("2.Create necklace with half precious stones");
                            int choice1 = in.nextInt();
                            System.out.println("Enter the number of stones on a necklace");
                            quantityStone = in.nextInt();
                            batya.createNecklace(necklace1, quantityStone, choiceStone, choice1);
                            System.out.println(necklace1 + "\n");
                            System.out.println("GET DESCRIPTION enter positions: ");
                            necklace1.getStone(in.nextInt() - 1).infoAboutStone();
                            break;


                }
                break;
            }

            case 2: {
                System.out.println("Total carat weight");
                System.out.println(Calculate.calculateTotalCaratWeight(necklace1.getStonesList()) + " carat" + "\n");
                break;
            }
            case 3: {
                System.out.println("Total cost in kzt");
                System.out.println(Calculate.calculateTotalStonesCost(necklace1.getStonesList()) + " kzt" + "\n");
                break;
            }
        }
    }
}
}
