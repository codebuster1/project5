/**
 * This is a class for Full Precious kz.aitu.oop.practice.practice5.Stone.
 * It inherits the stones.
 */
package kz.aitu.oop.practice.practice5.Stone;

import kz.aitu.oop.practice.practice5.SpecificationsStone.FullPreciousStoneType;
import kz.aitu.oop.practice.practice5.SpecificationsStone.StoneColor;

public class FullPreciousStone extends Stone {

    private FullPreciousStoneType name;

    public FullPreciousStone() {
        super();
    }


    public FullPreciousStone(double weight, double price, StoneColor color, FullPreciousStoneType name) {
        super(weight, price, color);
        setName(name);
    }

    public FullPreciousStoneType getName() {
        return name;
    }

    public void setName(FullPreciousStoneType name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "\nPreciousStone: " +
                "name : " + this.getName() + " description :" +
                ", color : " + this.getColor() +
                ", weight : " + String.format("%8.3f", this.getWeight()) +
                ", price : " + String.format("%8.2f", this.getPrice()) + "kzt" +
                '\n' + '\n';
    }
    //infoAboutStone this is an implementation of the IStoneInfo function.
    // It gives information about a particular type of stone.
    @Override
    public void infoAboutStone() {
        System.out.println("Description Full Precious  : It's "+getName()+" price of this stone:"+getPrice()+" KZT (original)");
    }
}
