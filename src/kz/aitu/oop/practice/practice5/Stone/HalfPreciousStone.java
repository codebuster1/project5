/**
 * this is a class for Half Precious kz.aitu.oop.practice.practice5.Stone.
 * it inherits the stones.
 */
package kz.aitu.oop.practice.practice5.Stone;

import kz.aitu.oop.practice.practice5.SpecificationsStone.HalfPreciousStoneType;
import kz.aitu.oop.practice.practice5.SpecificationsStone.Sparkle;
import kz.aitu.oop.practice.practice5.SpecificationsStone.StoneColor;


public class HalfPreciousStone extends Stone  {
    private HalfPreciousStoneType name;
    private Sparkle sparkle;

    public HalfPreciousStone(double weight, double price, StoneColor color) {
        super();
    }


    public HalfPreciousStone(double weight, double price, StoneColor color, HalfPreciousStoneType name, Sparkle sparkle) {
        super(weight, price, color);
        setName(name);
        setSparkle(sparkle);
    }

    public HalfPreciousStoneType getName() {
        return name;
    }

    public void setName(HalfPreciousStoneType name) {
        this.name = name;
    }

    public Sparkle getSparkle() {
        return sparkle;
    }

    public void setSparkle(Sparkle sparkle) {
        this.sparkle = sparkle;
    }


    @Override
    public String toString() {
        return "\nSemipreciousStone: " +
                "name : " + this.getName() +
                ", color : " + this.getColor() +
                ", weight : " + String.format("%8.3f", this.getWeight()) +
                ", price : " + String.format("%8.2f", this.getPrice()) + "kzt" +
                '\n' + '\n';
    }

    //infoAboutStone this is an implementation of the IStoneInfo function.
    // It gives information about a particular type of stone.
    @Override
    public void infoAboutStone() {
        System.out.println("Description Half Precious  : It's "+getName()+" price of this stone:"+getPrice()+" KZT (bijouterie)");
    }
}
