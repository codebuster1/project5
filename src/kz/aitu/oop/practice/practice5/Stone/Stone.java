/**
 * This is the main class for stones.
 * It will implement IStoneInfo.
 */
package kz.aitu.oop.practice.practice5.Stone;

import kz.aitu.oop.practice.practice5.SpecificationsStone.StoneColor;

public abstract class Stone implements IStoneInfo {
    private double weight;
    private double price;
    private StoneColor color;

    public Stone() {

    }

    public Stone(double weight, double price, StoneColor color) {
        setWeight(weight);
        setPrice(price);
        setColor(color);
    }

    public abstract void infoAboutStone();

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setColor(StoneColor color) {
        this.color = color;
    }

    public StoneColor getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "color - " + color + ", " + "weight - " + weight + ", " + "price - " + price + "kzt" + '\n' + '\n';
    }

}
